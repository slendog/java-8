package sk.slendog;

import java.io.IOException;


public class Main
{

    public static void main(String[] args) throws IOException, InterruptedException {


//        int a = 22;
//        byte b;
//        String c = Integer.toString(a);
//        c = String.format("%4s",Integer.toBinaryString(Integer.parseInt("" + c.charAt(c.length()-2)))).replace(' ', '0') + String.format("%4s",Integer.toBinaryString(Integer.parseInt("" + c.charAt(c.length()-1)))).replace(' ', '0');
//        System.out.println(Byte.parseByte(c, 2));

        Chip8FrontEnd c = new Chip8FrontEnd();

        while(true)
        {
            c.run();
        }



//        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
//
//            @Override
//            public boolean dispatchKeyEvent(KeyEvent ke) {
//                synchronized (Main.class) {
//                    switch (ke.getID()) {
//                        case KeyEvent.KEY_PRESSED:
//                            if (ke.getKeyCode() == KeyEvent.VK_A) {
//                                a = true;
//                                System.out.println("A is pressed");
//                            }
//                            break;
//
//                        case KeyEvent.KEY_RELEASED:
//                            if (ke.getKeyCode() == KeyEvent.VK_A) {
//                                a = false;
//                                System.out.println("A is released");
//                            }
//                            break;
//                    }
//                    return false;
//                }
//            }
//        });



//        System.out.println("Hello");
//
//        getKey();
//
//        System.out.println("Hello");


//        byte b = (byte)(128>>1);
//        System.out.println(binary(b));
//        System.out.println(Byte.toUnsignedInt(b));
//        System.out.println(binary((byte)(Byte.toUnsignedInt(b)>>1)));
//        System.out.println(Byte.toUnsignedInt((byte)(Byte.toUnsignedInt(b)>>1)));


        //https://stackoverflow.com/questions/63515194/how-to-run-a-code-60-times-per-second-in-java
//        long lastTime = System.nanoTime();
//        long secondStart = System.nanoTime();
//        final double ns = 1000000000.0 / 60.0;
//        double delta = 0;
//        int i = 0;
//        while(true){
//            long now = System.nanoTime();
//            delta += (now - lastTime) / ns;
//            lastTime = now;
//            while(delta >= 1){
////                System.out.println("Cycle: " + i);
//                delta--;
//                i++;
//            }
//
//            if((System.nanoTime() - secondStart) >= 1000000000)
//            {
//                System.out.println("FPS:" +  i);
//                i = 0;
//                secondStart=System.nanoTime();
//            }
//        }

//        final long start = System.currentTimeMillis();
//        long lastTick = start - 1;
//        int o = 1;
//        for(long i = System.currentTimeMillis(); i - start < 1000; i = System.currentTimeMillis())
//        {
//
//            if(((i - start) % 17 == 0) && i - start != lastTick)
//            {
//                System.out.println(o + ": Time since start: " + (i - start) + " miliseconds");
//                lastTick = i - start;
//                o++;
//            }
//        }

//        byte[] b =Files.readAllBytes(Path.of("/mnt/Data/Projects/Java/Development/chip-8/chip-8/BLINKY"));
//        Byte[] file = new Byte[b.length];
//
//        for(int i = 0; i < b.length; i++)
//        {
//            file[i] = b[i];
//        }
//
//        for(int i = 0; i < file.length; i+=2)
//        {
//            System.out.println("OpCode " + i/2 + " : " + String.format("%02X", Byte.toUnsignedInt(file[i])) + String.format("%02X", Byte.toUnsignedInt(file[i+1])));
//        }
    }

//    private static void getKey()
//    {
//        while(true)
//        {
//            for(int i = 0; i < 1; i++)
//            {
//                if(a)
//                {
//
//                    return;
//                }
//            }
//        }
//    }

    public static String binary(Byte b)
    {
        return String.format("%8s", Integer.toBinaryString(Byte.toUnsignedInt(b))).replace(' ', '0');
    }
}
