package sk.slendog;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class Chip8Graphics extends JPanel {

    private final Boolean[][] screen = new Boolean[64][32];
    private BufferedImage bImage;
    private final Integer multiplier;


    public Chip8Graphics(Integer multiplier)
    {
        this.multiplier = multiplier;
        bImage = new BufferedImage((multiplier*64) + 50, (multiplier*32) + 100, BufferedImage.TYPE_INT_RGB);
        Graphics g = bImage.getGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0,0,(multiplier*64) + 50,(multiplier*32) + 100);
        g.dispose();
        clearScreen();
    }

    public boolean drawSprite(Integer x, Integer y, Byte[] sprite)
    {
        boolean pixelCollision;
        pixelCollision = false;

        for(int i = 0; i < sprite.length; i++)
        {
            for(int o = 0; o < 8; o++)
            {
                if((x+o) < 64 && (y+i) < 32)
                {
                    if (String.format("%8s", Integer.toBinaryString(Byte.toUnsignedInt(sprite[i]))).replace(' ', '0').charAt(o) == '1')
                    {
                        if (screen[x + o][y + i])
                        {
                            pixelCollision = true;
                        }
                        screen[x + o][y + i] = !screen[x + o][y + i];
                    }
                }
            }
        }

        screenRedraw();

        return pixelCollision;
    }

    public void screenRedraw()
    {
        //redraw the screen with the content of screen[][]
        Graphics g = bImage.getGraphics();

        g.setColor(Color.BLACK);
        g.fillRect(0,0,(multiplier*64) + 50,(multiplier*32) + 100);

        for(int x = 0; x < screen.length; x++)
        {
            for(int y = 0; y < screen[x].length; y++)
            {
                if(screen[x][y])
                {
                    g.setColor(Color.WHITE);
                    g.fillRect(x* multiplier + 25, y* multiplier + 25, multiplier, multiplier);
                }
            }
        }
        g.setColor(Color.WHITE);
        g.drawRect(25,25,64* multiplier, 32* multiplier);


        g.dispose();

        getGraphics().drawImage(bImage,0,0,null);

    }

    public void clearScreen()
    {
        for (Boolean[] booleans : screen) {
            Arrays.fill(booleans, false);
        }
    }


    @Override
    public void update(Graphics g) {
        super.update(g);

        screenRedraw();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        screenRedraw();
    }

    @Override
    public void repaint(Rectangle r) {
        super.repaint(r);

        screenRedraw();
    }
}
