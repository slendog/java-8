package sk.slendog;

/*
HELP: Integer.decode("0xXXXX"); dokaze urobit z hex string prevod na cislo. podstatne je ze musi byt 0x!
 */

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Random;

public class Chip8System {

    private Boolean memoryLoaded = false;           //Memory load flag
    private Boolean[] keys = new Boolean[0x10];      //Key press state
    private Byte[] mem4k = new Byte[4096];          //4k memory (0x000 - 0xFFF)
    private Byte[] V = new Byte[16];                //VX registers
    private Integer delayTimer = 0;                 //Timer countdown to 0 at 60Hz
    private Integer soundTimer = 0;                 //Sound timer, counts down at 60Hz, when counter hits 0, speaker beeps
    private Integer programCounter = 0x200;         //Program counter starts at memory address 0x200
    private Integer I = 0;                          //Index register (memory pointer)
    private String opCode = "0000";                 //Current OpCode
    private final Random rand = new Random();       //Pseudorandom number generator
    private Chip8Graphics graphics;                 //Graphics

    //Characters for debug purpose

    /*  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0  */
    private final Byte[] spr0 = { (byte)0xF0, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0xF0 };

    /*  0x20, 0x60, 0x20, 0x20, 0x70, // 1  */
    private final Byte[] spr1 = { 0x20, 0x60, 0x20, 0x20, 0x70 };

    /*  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2  */
    private final Byte[] spr2 = { (byte)0xF0, 0x10, (byte)0xF0, (byte)0x80, (byte)0xF0 };

    /*  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3  */
    private final Byte[] spr3 = { (byte)0xF0, 0x10, (byte)0xF0, 0x10, (byte)0xF0 };

    /*  0x90, 0x90, 0xF0, 0x10, 0x10, // 4  */
    private final Byte[] spr4 = { (byte)0x90, (byte)0x90, (byte)0xF0, 0x10, 0x10 };

    /*  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5  */
    private final Byte[] spr5 = { (byte)0xF0, (byte)0x80, (byte)0xF0, 0x10, (byte)0xF0 };

    /*  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6  */
    private final Byte[] spr6 = { (byte)0xF0, (byte)0x80, (byte)0xF0, (byte)0x90, (byte)0xF0 };

    /*  0xF0, 0x10, 0x20, 0x40, 0x40, // 7  */
    private final Byte[] spr7 = { (byte)0xF0, 0x10, 0x20, 0x40, 0x40 };

    /*  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8  */
    private final Byte[] spr8 = { (byte)0xF0, (byte)0x90, (byte)0xF0, (byte)0x90, (byte)0xF0 };

    /*  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9  */
    private final Byte[] spr9 = { (byte)0xF0, (byte)0x90, (byte)0xF0, 0x10, (byte)0xF0 };

    /*  0xF0, 0x90, 0xF0, 0x90, 0x90, // A  */
    private final Byte[] spra = { (byte)0xF0, (byte)0x90, (byte)0xF0, (byte)0x90, (byte)0x90 };

    /*  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B  */
    private final Byte[] sprb = { (byte)0xE0, (byte)0x90, (byte)0xE0, (byte)0x90, (byte)0xE0 };

    /*  0xF0, 0x80, 0x80, 0x80, 0xF0, // C  */
    private final Byte[] sprc = { (byte)0xF0, (byte)0x80, (byte)0x80, (byte)0x80, (byte)0xF0 };

    /*  0xE0, 0x90, 0x90, 0x90, 0xE0, // D  */
    private final Byte[] sprd = { (byte)0xE0, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0xE0 };

    /*  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E  */
    private final Byte[] spre = { (byte)0xF0, (byte)0x80, (byte)0xF0, (byte)0x80, (byte)0xF0 };

    /*  0xF0, 0x80, 0xF0, 0x80, 0x80  // F  */
    private final Byte[] sprf = { (byte)0xF0, (byte)0x80, (byte)0xF0, (byte)0x80, (byte)0x80 };


    public Chip8System(Chip8Graphics g) throws IOException
    {
        graphics = null;
        graphics = g;

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new Kboard());
    }

    public Chip8System(Chip8Graphics g, String filepath, boolean debug) throws IOException
    {
//        byte[] temp = Files.readAllBytes(Path.of(filepath));
//
//        if (temp.length > mem4k.length - 0x200)
//        {
//            System.out.println("Chip out of memory. No file loaded!");
//        } else {
//            for(int i = 0; i < temp.length; i++)
//            {
//                mem4k[0x200+i] = temp[i];
//            }
//            memoryLoaded=true;
//        }

        setFont();

        graphics = null;
        graphics = g;

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new Kboard());
    }

    public void setFile(String filepath) throws IOException
    {
        byte[] temp = Files.readAllBytes(Path.of(filepath));

        Arrays.fill(mem4k, (byte)0);

        if (temp.length > mem4k.length - 0x200)
        {
            System.out.println("Chip out of memory. No file loaded!");
        } else {
            for(int i = 0; i < temp.length; i++)
            {
                mem4k[0x200+i] = temp[i];
            }
            memoryLoaded=true;
        }

        Arrays.fill(V, (byte)0);
        Arrays.fill(keys, false);

        setFont();
    }

    public void startEmulation()
    {
        if(memoryLoaded) {

            //Timing system that makes code run N times per second. //Thanks so much gentleman in this post https://stackoverflow.com/questions/63515194/how-to-run-a-code-60-times-per-second-in-java
            double N = 60.0;
            long lastTime = System.nanoTime();
            final double ns = 1000000000.0 / N;
            double delta = 0;
            while(true) {
                long now = System.nanoTime();
                delta += (now - lastTime) / ns;
                lastTime = now;
                while (delta >= 1) {

                    nextCycle();

                    switch (opCode.charAt(0)) {
                    /*
                    0NNN 	Call 		Calls machine code routine (RCA 1802 for COSMAC VIP) at address NNN. Not necessary for most ROMs.
                    00E0 	Display 	disp_clear() 	Clears the screen.
                    00EE 	Flow 	return; 	Returns from a subroutine.
                    1NNN 	Flow 	goto NNN; 	Jumps to address NNN.
                    2NNN 	Flow 	*(0xNNN)() 	Calls subroutine at NNN.
                    3XNN 	Cond 	if(Vx==NN) 	Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block)
                    4XNN 	Cond 	if(Vx!=NN) 	Skips the next instruction if VX does not equal NN. (Usually the next instruction is a jump to skip a code block)
                    5XY0 	Cond 	if(Vx==Vy) 	Skips the next instruction if VX equals VY. (Usually the next instruction is a jump to skip a code block)
                    6XNN 	Const 	Vx = NN 	Sets VX to NN.
                    7XNN 	Const 	Vx += NN 	Adds NN to VX. (Carry flag is not changed)
                    8XY0 	Assign 	Vx=Vy 	Sets VX to the value of VY.
                    8XY1 	BitOp 	Vx=Vx|Vy 	Sets VX to VX or VY. (Bitwise OR operation)
                    8XY2 	BitOp 	Vx=Vx&Vy 	Sets VX to VX and VY. (Bitwise AND operation)
                    8XY3 	BitOp 	Vx=Vx^Vy 	Sets VX to VX xor VY.
                    8XY4 	Math 	Vx += Vy 	Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there is not.
                    8XY5 	Math 	Vx -= Vy 	VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there is not.
                    8XY6 	BitOp 	Vx>>=1 	Stores the least significant bit of VX in VF and then shifts VX to the right by 1.
                    8XY7 	Math 	Vx=Vy-Vx 	Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there is not.
                    8XYE 	BitOp 	Vx<<=1 	Stores the most significant bit of VX in VF and then shifts VX to the left by 1.
                    9XY0 	Cond 	if(Vx!=Vy) 	Skips the next instruction if VX does not equal VY. (Usually the next instruction is a jump to skip a code block)
                    ANNN 	MEM 	I = NNN 	Sets I to the address NNN.
                    BNNN 	Flow 	PC=V0+NNN 	Jumps to the address NNN plus V0.
                    CXNN 	Rand 	Vx=rand()&NN 	Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
                    DXYN 	Disp 	draw(Vx,Vy,N) 	Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N+1 pixels. Each row of 8 pixels is read as bit-coded starting from memory location I; I value does not change after the execution of this instruction. As described above, VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn, and to 0 if that does not happen
                    EX9E 	KeyOp 	if(key()==Vx) 	Skips the next instruction if the key stored in VX is pressed. (Usually the next instruction is a jump to skip a code block)
                    EXA1 	KeyOp 	if(key()!=Vx) 	Skips the next instruction if the key stored in VX is not pressed. (Usually the next instruction is a jump to skip a code block)
                    FX07 	Timer 	Vx = get_delay() 	Sets VX to the value of the delay timer.
                    FX0A 	KeyOp 	Vx = get_key() 	A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event)
                    FX15 	Timer 	delay_timer(Vx) 	Sets the delay timer to VX.
                    FX18 	Sound 	sound_timer(Vx) 	Sets the sound timer to VX.
                    FX1E 	MEM 	I +=Vx 	Adds VX to I. VF is not affected.[c]
                    FX29 	MEM 	I=sprite_addr[Vx] 	Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                    FX33 	BCD 	set_BCD(Vx);    Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. (In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2.)
                                     *(I+0)=BCD(3);
                                     *(I+1)=BCD(2);
                                     *(I+2)=BCD(1);
                    FX55 	MEM 	reg_dump(Vx,&I) 	Stores V0 to VX (including VX) in memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified.[d]
                    FX65 	MEM 	reg_load(Vx,&I) 	Fills V0 to VX (including VX) with values from memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified.[d]
                     */
                        case '0':
                            switch ("" + opCode.charAt(2) + opCode.charAt(3)) {
                                case "E0":
                                    //00E0 	Display 	disp_clear() 	Clears the screen.
                                    //TODO: make a debug message
                                    graphics.clearScreen();
                                    continue;
                                case "EE":
                                    //00EE 	Flow 	return; 	Returns from a subroutine.
                                    //TODO: make a debug message
                                    System.out.println("Returned from a subroutine");
                                    return;
                                default:
                                    //TODO: 0NNN 	Call 		Calls machine code routine (RCA 1802 for COSMAC VIP) at address NNN. Not necessary for most ROMs.
                                    //TODO: make a debug message
                                    System.out.println("Call machine code at 0x" + opCode.charAt(1) + opCode.charAt(2) + opCode.charAt(3));
                                    continue;
                            }
                        case '1':
                            //1NNN 	Flow 	goto NNN; 	Jumps to address NNN.
                            //TODO: make a debug message
                            programCounter = Integer.decode("0x" + opCode.charAt(1) + opCode.charAt(2) + opCode.charAt(3));
                            continue;
                        case '2':
                            //2NNN 	Flow 	*(0xNNN)() 	Calls subroutine at NNN.
                            //TODO: make a debug message
                            int stack = programCounter;
                            programCounter = Integer.decode("0x" + opCode.charAt(1) + opCode.charAt(2) + opCode.charAt(3));
                            System.out.println("Call subroutine at 0x" + opCode.charAt(1) + opCode.charAt(2) + opCode.charAt(3));
                            startEmulation();
                            programCounter = stack;
                            continue;
                        case '3':
                            //3XNN 	Cond 	if(Vx==NN) 	Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block)
                            //TODO: make a debug message
                            if (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) == Integer.decode("0x" + opCode.charAt(2) + opCode.charAt(3))) {
                                programCounter += 2;
                            }
                            continue;
                        case '4':
                            //4XNN 	Cond 	if(Vx!=NN) 	Skips the next instruction if VX does not equal NN. (Usually the next instruction is a jump to skip a code block)
                            //TODO: make a debug message
                            if (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) != Integer.decode("0x" + opCode.charAt(2) + opCode.charAt(3))) {
                                programCounter += 2;
                            }
                            continue;
                        case '5':
                            //5XY0 	Cond 	if(Vx==Vy) 	Skips the next instruction if VX equals VY. (Usually the next instruction is a jump to skip a code block)
                            //TODO: make a debug message
                            if (opCode.charAt(3) == '0') {
                                if (V[Integer.decode("0x" + opCode.charAt(1))].equals(V[Integer.decode("0x" + opCode.charAt(2))])) {
                                    programCounter += 2;
                                }
                            } else {
                                //TODO: make a debug message
                                System.out.println("Unknown OpCode: 0x" + opCode);
                            }
                            continue;
                        case '6':
                            //6XNN 	Const 	Vx = NN 	Sets VX to NN.
                            //TODO: make a debug message
                            V[Integer.decode("0x" + opCode.charAt(1))] = Integer.decode("0x" + opCode.charAt(2) + opCode.charAt(3)).byteValue();
                            continue;
                        case '7':
                            //7XNN 	Const 	Vx += NN 	Adds NN to VX. (Carry flag is not changed)
                            //TODO: make a debug message
                            V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (V[Integer.decode("0x" + opCode.charAt(1))] + Integer.decode("0x" + opCode.charAt(2) + opCode.charAt(3)).byteValue());
                            continue;
                        case '8':
                            switch (opCode.charAt(3)) {
                                case '0':
                                    //8XY0 	Assign 	Vx=Vy 	Sets VX to the value of VY.
                                    //TODO: make a debug message
                                    V[Integer.decode("0x" + opCode.charAt(1))] = V[Integer.decode("0x" + opCode.charAt(2))];
                                    continue;
                                case '1':
                                    //8XY1 	BitOp 	Vx=Vx|Vy 	Sets VX to VX or VY. (Bitwise OR operation)
                                    //TODO: make a debug message
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (V[Integer.decode("0x" + opCode.charAt(1))] | V[Integer.decode("0x" + opCode.charAt(2))]);
                                    continue;
                                case '2':
                                    //8XY2 	BitOp 	Vx=Vx&Vy 	Sets VX to VX and VY. (Bitwise AND operation)
                                    //TODO: make a debug message
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (V[Integer.decode("0x" + opCode.charAt(1))] & V[Integer.decode("0x" + opCode.charAt(2))]);
                                    continue;
                                case '3':
                                    //8XY3 	BitOp 	Vx=Vx^Vy 	Sets VX to VX xor VY.
                                    //TODO: make a debug message
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (V[Integer.decode("0x" + opCode.charAt(1))] ^ V[Integer.decode("0x" + opCode.charAt(2))]);
                                    continue;
                                case '4':
                                    //8XY4 	Math 	Vx += Vy 	Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there is not.
                                    //TODO: make a debug message
                                    if (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) + Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(2))]) >= 0x100) {
                                        V[0xf] = 1;
                                    } else {
                                        V[0xf] = 0;
                                    }
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (V[Integer.decode("0x" + opCode.charAt(1))] + V[Integer.decode("0x" + opCode.charAt(2))]);
                                    continue;
                                case '5':
                                    //8XY5 	Math 	Vx -= Vy 	VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there is not.
                                    //TODO: make a debug message
                                    if (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) - Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(2))]) < 0) {
                                        V[0xf] = 0;
                                    } else {
                                        V[0xf] = 1;
                                    }
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (V[Integer.decode("0x" + opCode.charAt(1))] - V[Integer.decode("0x" + opCode.charAt(2))]);
                                    continue;
                                case '6':
                                    //8XY6 	BitOp 	Vx>>=1 	Stores the least significant bit of VX in VF and then shifts VX to the right by 1.
                                    //TODO: make a debug message
                                    V[0xf] =(byte) (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) % 2);
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) >> 1);
                                    continue;
                                case '7':
                                    //8XY7 	Math 	Vx=Vy-Vx 	Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there is not.
                                    //TODO: make a debug message
                                    if (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(2))]) - Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) < 0) {
                                        V[0xf] = 0;
                                    } else {
                                        V[0xf] = 1;
                                    }
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (V[Integer.decode("0x" + opCode.charAt(2))] + V[Integer.decode("0x" + opCode.charAt(1))]);
                                    continue;
                                case 'E':
                                    //8XYE 	BitOp 	Vx<<=1 	Stores the most significant bit of VX in VF and then shifts VX to the left by 1.
                                    //TODO: make a debug message
                                    if(Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) >= 128)
                                    {
                                        V[0xf] = 1;
                                    } else {
                                        V[0xf] = 0;
                                    }
                                    V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]) >> 1);
                                    continue;
                                default:
                                    //TODO: make a debug message
                                    System.out.println("Unknown OpCode: 0x" + opCode);
                                    continue;
                            }
                        case '9':
                            //9XY0 	Cond 	if(Vx!=Vy) 	Skips the next instruction if VX does not equal VY. (Usually the next instruction is a jump to skip a code block)
                            //TODO: make a debug message
                            if (!V[Integer.decode("0x" + opCode.charAt(1))].equals(V[Integer.decode("0x" + opCode.charAt(2))])) {
                                programCounter += 2;
                            }
                            continue;
                        case 'A':
                            //ANNN 	MEM 	I = NNN 	Sets I to the address NNN.
                            //TODO: make a debug message
                            I = Integer.decode("0x" + opCode.charAt(1) + opCode.charAt(2) + opCode.charAt(3));
                            continue;
                        case 'B':
                            //BNNN 	Flow 	PC=V0+NNN 	Jumps to the address NNN plus V0.
                            //TODO: make a debug message
                            programCounter = Byte.toUnsignedInt(V[0]) + Integer.decode("0x" + opCode.charAt(1) + opCode.charAt(2) + opCode.charAt(3));
                            continue;
                        case 'C':
                            //CXNN 	Rand 	Vx=rand()&NN 	Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
                            //TODO: make a debug message
                            V[Integer.decode("0x" + opCode.charAt(1))] = (byte) (getRand() & Integer.decode("0x" + opCode.charAt(2) + opCode.charAt(3)));
                            continue;
                        case 'D':
                            //DXYN 	Disp 	draw(Vx,Vy,N) 	Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N+1 pixels. Each row of 8 pixels is read as bit-coded starting from memory location I; I value does not change after the execution of this instruction. As described above, VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn, and to 0 if that does not happen
                            //TODO: make a debug message
                            boolean pixel;
                            Byte[] sprite = new Byte[Integer.decode("0x" + opCode.charAt(3))];
                            System.arraycopy(mem4k, I, sprite, 0, sprite.length);
                            pixel = graphics.drawSprite(Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]), Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(2))]), sprite);
                            if(pixel)
                            {
                                V[0xF] = 1;
                            } else {
                                V[0xF] = 0;
                            }
                            continue;
                        case 'E':
                            switch ("" + opCode.charAt(2) + opCode.charAt(3)) {
                                case "9E":
                                    //EX9E 	KeyOp 	if(key()==Vx) 	Skips the next instruction if the key stored in VX is pressed. (Usually the next instruction is a jump to skip a code block)
                                    //TODO: make a debug message
                                    if(keys[Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))])])
                                    {
                                        programCounter+=2;
                                    }
                                    continue;
                                case "A1":
                                    //EXA1 	KeyOp 	if(key()!=Vx) 	Skips the next instruction if the key stored in VX is not pressed. (Usually the next instruction is a jump to skip a code block)
                                    //TODO: make a debug message
                                    if(!keys[Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))])])
                                    {
                                        programCounter+=2;
                                    }
                                    continue;
                                default:
                                    //TODO: make a debug message
                                    System.out.println("Unknown OpCode: 0x" + opCode);
                                    continue;
                            }
                        case 'F':
                            switch ("" + opCode.charAt(2) + opCode.charAt(3)) {
                                case "07":
                                    //FX07 	Timer 	Vx = get_delay() 	Sets VX to the value of the delay timer.
                                    //TODO: make a debug message
                                    V[Integer.decode("0x" + opCode.charAt(1))] = delayTimer.byteValue();
                                    System.out.println("Set the V" + opCode.charAt(1) + " to delayTimer: " + delayTimer);
                                    continue;
                                case "0A":
                                    //FX0A 	KeyOp 	Vx = get_key() 	A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event)
                                    //TODO: make a debug message
                                    V[Integer.decode("0x" + opCode.charAt(1))] = getKey();
                                    continue;
                                case "15":
                                    //FX15 	Timer 	delay_timer(Vx) 	Sets the delay timer to VX.
                                    //TODO: make a debug message
                                    delayTimer = Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]);
                                    System.out.println("Delay timer set to V" + opCode.charAt(1) + ": " + V[Integer.decode("0x" + opCode.charAt(1))]);
                                    continue;
                                case "18":
                                    //FX18 	Sound 	sound_timer(Vx) 	Sets the sound timer to VX.
                                    //TODO: make a debug message
                                    soundTimer = Byte.toUnsignedInt(V[Integer.decode("0x" + opCode.charAt(1))]);
                                    System.out.println("Sound timer set to V" + opCode.charAt(1) + ": " + V[Integer.decode("0x" + opCode.charAt(1))]);
                                    continue;
                                case "1E":
                                    //FX1E 	MEM 	I +=Vx 	Adds VX to I. VF is not affected.
                                    //TODO: make a debug message
                                    I += V[Integer.decode("0x" + opCode.charAt(1))];
                                    continue;
                                case "29":
                                    //FX29 	MEM 	I=sprite_addr[Vx] 	Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                                    //TODO: make a debug message
                                    switch (V[Integer.decode("0x"+ opCode.charAt(1))])
                                    {
                                        case (byte)0x00:
                                            I = 0x00;  //0
                                            continue;
                                        case (byte)0x01:
                                            I = 0x05;  //5
                                            continue;
                                        case (byte)0x02:
                                            I = 0x0A;  //10
                                            continue;
                                        case (byte)0x03:
                                            I = 0x0F;  //15
                                            continue;
                                        case (byte)0x04:
                                            I = 0x14;  //20
                                            continue;
                                        case (byte)0x05:
                                            I = 0x19;  //25
                                            continue;
                                        case (byte)0x06:
                                            I = 0x1E;  //30
                                            continue;
                                        case (byte)0x07:
                                            I = 0x23;  //35
                                            continue;
                                        case (byte)0x08:
                                            I = 0x28;  //40
                                            continue;
                                        case (byte)0x09:
                                            I = 0x2D;  //45
                                            continue;
                                        case (byte)0x0A:
                                            I = 0x32;  //50
                                            continue;
                                        case (byte)0x0B:
                                            I = 0x37;  //55
                                            continue;
                                        case (byte)0x0C:
                                            I = 0x3C;  //60
                                            continue;
                                        case (byte)0x0D:
                                            I = 0x41;  //65
                                            continue;
                                        case (byte)0x0E:
                                            I = 0x46;  //70
                                            continue;
                                        case (byte)0x0F:
                                            I = 0x4B;  //75
                                            continue;
                                        default:
                                            System.out.println("Unknown character: " + Integer.toHexString(Byte.toUnsignedInt(V[Integer.decode("0x"+ opCode.charAt(1))])).toUpperCase());
                                    }
                                    continue;
                                case "33":
                                    //FX33 	BCD 	set_BCD(Vx);    Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. (In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2.)
                                    //              *(I+0)=BCD(3);
                                    //              *(I+1)=BCD(2);
                                    //              *(I+2)=BCD(1);
                                    //TODO: make a debug message
                                    makeBCD(Integer.decode("0x" + opCode.charAt(1)));
                                    continue;
                                case "55":
                                    //FX55 	MEM 	reg_dump(Vx,&I) 	Stores V0 to VX (including VX) in memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified.
                                    //TODO: make a debug message
                                    regDump(Integer.decode("0x" + opCode.charAt(1)));
                                    continue;
                                case "65":
                                    //FX65 	MEM 	reg_load(Vx,&I) 	Fills V0 to VX (including VX) with values from memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified.
                                    //TODO: make a debug message
                                    regLoad(Integer.decode("0x" + opCode.charAt(1)));
                                    continue;
                                default:
                                    //TODO: make a debug message
                                    System.out.println("Unknown OpCode: 0x" + opCode);
                                    continue;
                            }
                        default:
                            System.out.println("Unknown OpCode: 0x" + opCode);
                    }

                    delta--;
                }
            }

        } else {
            System.out.println("Nothing loaded into memory!");
        }
    }

    private void nextCycle()
    {
        if(delayTimer > 0) {
            delayTimer--;
        }

        if(soundTimer != 0)
        {
            //TODO: make this actually beep!
            System.out.println("BEEP");
            soundTimer--;
        }

//        graphics.screenRedraw();

        if(programCounter < 0xFFF) {
            opCode = String.format("%02X", Byte.toUnsignedInt(mem4k[programCounter])) + String.format("%02X", Byte.toUnsignedInt(mem4k[programCounter + 1]));
            opCode = opCode.toUpperCase();
            programCounter += 2;
        } else {
            System.out.println("Oops! rom went out of memory during normal cycle.");
        }
    }

    private Byte getRand()
    {
        byte b;
        b = (byte)rand.nextInt(256);
        rand.setSeed(rand.nextLong());
        return b;
    }

    public Boolean clearMemory()
    {
        for(int i = 0x200; i < mem4k.length; i++)
        {
            mem4k[i] = null;
        }
        memoryLoaded=false;
        return true;
    }

    private void regDump(int VX)
    {
        for(int i = 0; i <= VX; i++)
        {
            mem4k[I+i] = V[i];
        }
    }

    private void regLoad(int VX)
    {
        for(int i = 0; i <= VX; i++)
        {
            V[i] = mem4k[I+i];
        }
    }

    private byte getKey()
    {
        //noinspection IdempotentLoopBody
        while(true)
        {
            for(int i = 0; i < 0x10; i++)
            {
                if(keys[i])
                {
                    return (byte) i;
                }
            }
        }
    }

    public void makeBCD(int e)
    {
        int num = Byte.toUnsignedInt(V[e]);

        String tmp;
        if(num < 10)
        {
            tmp = "00" + num;
        } else if(num < 100) {
            tmp = "0" + num;
        } else {
            tmp = Integer.toString(num);
        }

        mem4k[I] = Byte.parseByte(String.format("%8s",Integer.toBinaryString(Integer.parseInt("" + tmp.charAt(0)))).replace(' ', '0'), 2);
        mem4k[I+1] = Byte.parseByte(String.format("%8s",Integer.toBinaryString(Integer.parseInt("" + tmp.charAt(1)))).replace(' ', '0'), 2);
        mem4k[I+2] = Byte.parseByte(String.format("%8s",Integer.toBinaryString(Integer.parseInt("" + tmp.charAt(2)))).replace(' ', '0'), 2);
    }
    public void setGraphics(Chip8Graphics g)
    {
        graphics = null;
        graphics = g;
    }

    private void setFont()
    {
        //0 (0xF0, 0x90, 0x90, 0x90, 0xF0)
        mem4k[0] = (byte)0xF0;
        mem4k[1] = (byte)0x90;
        mem4k[2] = (byte)0x90;
        mem4k[3] = (byte)0x90;
        mem4k[4] = (byte)0xF0;

        //1 (0x20, 0x60, 0x20, 0x20, 0x70)
        mem4k[5] = (byte)0x20;
        mem4k[6] = (byte)0x60;
        mem4k[7] = (byte)0x20;
        mem4k[8] = (byte)0x20;
        mem4k[9] = (byte)0x70;

        //2 (0xF0, 0x10, 0xF0, 0x80, 0xF0)
        mem4k[10] = (byte)0xF0;
        mem4k[11] = (byte)0x10;
        mem4k[12] = (byte)0xF0;
        mem4k[13] = (byte)0x80;
        mem4k[14] = (byte)0xF0;

        //3 (0xF0, 0x10, 0xF0, 0x10, 0xF0)
        mem4k[15] = (byte)0xF0;
        mem4k[16] = (byte)0x10;
        mem4k[17] = (byte)0xF0;
        mem4k[18] = (byte)0x10;
        mem4k[19] = (byte)0xF0;

        //4 (0x90, 0x90, 0xF0, 0x10, 0x10)
        mem4k[20] = (byte)0x90;
        mem4k[21] = (byte)0x90;
        mem4k[22] = (byte)0xF0;
        mem4k[23] = (byte)0x10;
        mem4k[24] = (byte)0x10;

        //5 (0xF0, 0x80, 0xF0, 0x10, 0xF0)
        mem4k[25] = (byte)0xF0;
        mem4k[26] = (byte)0x80;
        mem4k[27] = (byte)0xF0;
        mem4k[28] = (byte)0x10;
        mem4k[29] = (byte)0xF0;

        //6 (0xF0, 0x80, 0xF0, 0x90, 0xF0)
        mem4k[30] = (byte)0xF0;
        mem4k[31] = (byte)0x80;
        mem4k[32] = (byte)0xF0;
        mem4k[33] = (byte)0x90;
        mem4k[34] = (byte)0xF0;

        //7 (0xF0, 0x10, 0x20, 0x40, 0x40)
        mem4k[35] = (byte)0xF0;
        mem4k[36] = (byte)0x10;
        mem4k[37] = (byte)0x20;
        mem4k[38] = (byte)0x40;
        mem4k[39] = (byte)0x40;

        //8 (0xF0, 0x90, 0xF0, 0x90, 0xF0)
        mem4k[40] = (byte)0xF0;
        mem4k[41] = (byte)0x90;
        mem4k[42] = (byte)0xF0;
        mem4k[43] = (byte)0x90;
        mem4k[44] = (byte)0xF0;

        //9 (0xF0, 0x90, 0xF0, 0x10, 0xF0)
        mem4k[45] = (byte)0xF0;
        mem4k[46] = (byte)0x90;
        mem4k[47] = (byte)0xF0;
        mem4k[48] = (byte)0x10;
        mem4k[49] = (byte)0xF0;

        //A (0xF0, 0x90, 0xF0, 0x90, 0x90)
        mem4k[50] = (byte)0xF0;
        mem4k[51] = (byte)0x90;
        mem4k[52] = (byte)0xF0;
        mem4k[53] = (byte)0x90;
        mem4k[54] = (byte)0x90;

        //B (0xE0, 0x90, 0xE0, 0x90, 0xE0)
        mem4k[55] = (byte)0xE0;
        mem4k[56] = (byte)0x90;
        mem4k[57] = (byte)0xE0;
        mem4k[58] = (byte)0x90;
        mem4k[59] = (byte)0xE0;

        //C (0xF0, 0x80, 0x80, 0x80, 0xF0)
        mem4k[60] = (byte)0xF0;
        mem4k[61] = (byte)0x80;
        mem4k[62] = (byte)0x80;
        mem4k[63] = (byte)0x80;
        mem4k[64] = (byte)0xF0;

        //D (0xE0, 0x90, 0x90, 0x90, 0xE0)
        mem4k[65] = (byte)0xE0;
        mem4k[66] = (byte)0x90;
        mem4k[67] = (byte)0x90;
        mem4k[68] = (byte)0x90;
        mem4k[69] = (byte)0xE0;

        //E (0xF0, 0x80, 0xF0, 0x80, 0xF0)
        mem4k[70] = (byte)0xF0;
        mem4k[71] = (byte)0x80;
        mem4k[72] = (byte)0xF0;
        mem4k[73] = (byte)0x80;
        mem4k[74] = (byte)0xF0;

        //F (0xF0, 0x80, 0xF0, 0x80, 0x80)
        mem4k[75] = (byte)0xF0;
        mem4k[76] = (byte)0x80;
        mem4k[77] = (byte)0xF0;
        mem4k[78] = (byte)0x80;
        mem4k[79] = (byte)0x80;
    }

    private class Kboard implements KeyEventDispatcher
    {
        @Override
        public boolean dispatchKeyEvent(KeyEvent ke) {
            synchronized (Chip8System.class) {
                switch (ke.getID()) {
                    case KeyEvent.KEY_PRESSED:
                        switch (ke.getKeyCode()) {
                            case KeyEvent.VK_1:
                                keys[0x1] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr1 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_2:
                                keys[0x2] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr2 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_3:
                                keys[0x3] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr3 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_4:
                                keys[0xC] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, sprc );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_Q:
                                keys[0x4] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr4 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_W:
                                keys[0x5] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr5 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_E:
                                keys[0x6] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr6 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_R:
                                keys[0xD] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, sprd );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_A:
                                keys[0x7] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr7 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_S:
                                keys[0x8] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr8 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_D:
                                keys[0x9] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr9 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_F:
                                keys[0xE] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spre );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_Z:
                                keys[0xA] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spra );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_X:
                                keys[0x0] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, spr0 );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_C:
                                keys[0xB] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, sprb );
//                                graphics.screenRedraw();
                                break;
                            case KeyEvent.VK_V:
                                keys[0xF] = true;
//                                System.out.println("registered");
//                                graphics.drawSprite(0,0, sprf );
//                                graphics.screenRedraw();
                                break;
                            default:
                                graphics.clearScreen();
                        }
                        break;

                    case KeyEvent.KEY_RELEASED:
                        switch (ke.getKeyCode()) {
                            case KeyEvent.VK_1:
                                keys[0x1] = false;
                                break;
                            case KeyEvent.VK_2:
                                keys[0x2] = false;
                                break;
                            case KeyEvent.VK_3:
                                keys[0x3] = false;
                                break;
                            case KeyEvent.VK_4:
                                keys[0xC] = false;
                                break;
                            case KeyEvent.VK_Q:
                                keys[0x4] = false;
                                break;
                            case KeyEvent.VK_W:
                                keys[0x5] = false;
                                break;
                            case KeyEvent.VK_E:
                                keys[0x6] = false;
                                break;
                            case KeyEvent.VK_R:
                                keys[0xD] = false;
                                break;
                            case KeyEvent.VK_A:
                                keys[0x7] = false;
                                break;
                            case KeyEvent.VK_S:
                                keys[0x8] = false;
                                break;
                            case KeyEvent.VK_D:
                                keys[0x9] = false;
                                break;
                            case KeyEvent.VK_F:
                                keys[0xE] = false;
                                break;
                            case KeyEvent.VK_Z:
                                keys[0xA] = false;
                                break;
                            case KeyEvent.VK_X:
                                keys[0x0] = false;
                                break;
                            case KeyEvent.VK_C:
                                keys[0xB] = false;
                                break;
                            case KeyEvent.VK_V:
                                keys[0xF] = false;
                                break;
                        }
                }
                return false;
            }
        }
    }

}