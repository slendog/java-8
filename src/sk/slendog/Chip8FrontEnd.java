package sk.slendog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Chip8FrontEnd {

    private String filepath;
    private Integer mult = 10;
    private Chip8Graphics g;
    private JFileChooser fileChooser;
    private boolean r = false;
    private Chip8System sys;


    public Chip8FrontEnd() throws InterruptedException, IOException {
        JFrame frame = new JFrame();
        frame.setLayout(new BorderLayout());

        fileChooser = new JFileChooser();
        Chip8Graphics panel;


        panel = new Chip8Graphics(mult);
        panel.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

        sys = new Chip8System(panel);

        JMenuBar bar = new JMenuBar();

        JMenu menu = new JMenu("File");

        JMenuItem item = new JMenuItem("Open");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = fileChooser.showOpenDialog(null);
                if(ret == JFileChooser.APPROVE_OPTION)
                {
                    filepath = fileChooser.getSelectedFile().getAbsolutePath();
                    System.out.println("This is filepath: " + filepath);
                }
            }
        });
        menu.add(item);

        item = null;
        item = new JMenuItem("Run");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(filepath != null)
                {
                    try {
                        sys.setFile(filepath);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    r = true;
                }
            }
        });

        menu.add(item);

        bar.add(menu);

        frame.setLayout(new BorderLayout());

        frame.add(panel);
        frame.setJMenuBar(bar);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setTitle("Java-8");
        frame.setSize(mult*64 + 50,mult*32 + 100);
//        frame.pack();
        panel.setBackground(Color.BLACK);
        frame.setVisible(true);

        Thread.sleep(1000);

//        sys.setFile("chip-8/BRIX");

        panel.screenRedraw();

//        sys.startEmulation();
    }

    public void run()
    {
        if(r)
        {
            sys.startEmulation();
            r = false;
        }
    }
}
